/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Librairie.dao;

import Librairie.model.Auteur;
import Librairie.model.Livre;
import Librairie.model.Categorie;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vagrant
 */
public class BookDAOImpl implements BookDAO {

	 static {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException ex) {
        }
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:postgresql://192.168.56.10:5432/librairiedb",
                "postgres", "postgres");
    }

	private void closeConnection(Connection connection) {
		if (connection == null)
			return;
		try {
			connection.close();
		} catch (SQLException ex) {
		}
	}

	public List<Livre> findAllBooks() {
		List<Livre> result = new ArrayList<Livre>();
		List<Auteur> auteurList = new ArrayList<Auteur>();

		String sql = "select * from livre inner join auteur on livre.id = auteur.livre_id";

		Connection connection = null;
		try {
			connection = getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Livre livre = new Livre();
				Auteur auteur = new Auteur();
				livre.setId(resultSet.getLong("id"));
                                livre.setIsbn(resultSet.getString("isbn"));
				livre.setLivre_titre(resultSet.getString("livre_titre"));
				livre.setCategorie_id(resultSet.getLong("categorie_id"));
                                livre.setPrix(resultSet.getDouble("prix"));
                                livre.setEditeur(resultSet.getString("editeur"));
				auteur.setLivre_id(resultSet.getLong("livre_id"));
				auteur.setNom(resultSet.getString("nom"));
				auteur.setPrenoms(resultSet.getString("prenoms"));
                                auteur.setTelephone(resultSet.getString("telephone"));
                                auteur.setPays(resultSet.getString("pays"));
                                auteur.setId(resultSet.getLong("id"));
                                auteur.setEmail(resultSet.getString("ville"));
                                auteur.setVille(resultSet.getString("email"));
				auteurList.add(auteur);
				livre.setAuteurs(auteurList);
				result.add(livre);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			closeConnection(connection);
		}
		return result;
	}

	@Override
	public List<Livre> searchBooksByKeyword(String keyWord) {
		List<Livre> result = new ArrayList<Livre>();
		List<Auteur> auteurList = new ArrayList<Auteur>();

		String sql = "select * from livre inner join auteur on livre.id = auteur.livre_id"
				+ " where livre_titre like '%"
				+ keyWord.trim()
				+ "%'"
				+ " or nom like '%"
				+ keyWord.trim()
				+ "%'"
				+ " or prenoms like '%" + keyWord.trim() + "%'";

		Connection connection = null;
		try {

			connection = getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Livre livre = new Livre();
				Auteur auteur = new Auteur();
				livre.setId(resultSet.getLong("id"));
                                livre.setIsbn(resultSet.getString("isbn"));
				livre.setLivre_titre(resultSet.getString("livre_titre"));
				livre.setCategorie_id(resultSet.getLong("categorie_id"));
                                livre.setPrix(resultSet.getDouble("prix"));
                                livre.setEditeur(resultSet.getString("editeur"));
				auteur.setLivre_id(resultSet.getLong("livre_id"));
				auteur.setNom(resultSet.getString("nom"));
				auteur.setPrenoms(resultSet.getString("prenoms"));
                                auteur.setTelephone(resultSet.getString("telephone"));
                                auteur.setPays(resultSet.getString("pays"));
                                auteur.setId(resultSet.getLong("id"));
                                auteur.setEmail(resultSet.getString("ville"));
                                auteur.setVille(resultSet.getString("email"));
				auteurList.add(auteur);
				livre.setAuteurs(auteurList);
				result.add(livre);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			closeConnection(connection);
		}

		return result;
	}

	public List<Categorie> findAllCategories() {
		List<Categorie> result = new ArrayList<>();
		String sql = "select * from categorie";

		Connection connection = null;
		try {
			connection = getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Categorie category = new Categorie();
				category.setId(resultSet.getLong("id"));
				category.setCategorie_description(resultSet
						.getString("categorie_description"));
				result.add(category);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			closeConnection(connection);
		}
		return result;
	}

	public void insert(Livre book) {
		Connection connection = null;
		try {
			connection = getConnection();
			PreparedStatement statement = connection.prepareStatement(
					"insert into livre (livre_titre) values (?)",
					Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, book.getLivre_titre());
			statement.execute();
			ResultSet generatedKeys = statement.getGeneratedKeys();
			if (generatedKeys.next()) {
				book.setId(generatedKeys.getLong(1));
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			closeConnection(connection);
		}
	}

	public void update(Livre book) {
	}

	public void delete(Long bookId) {
		Connection connection = null;

		try {
			connection = getConnection();
			PreparedStatement statement = connection
					.prepareStatement("delete from livre where id=?");
			statement.setLong(1, bookId);
			statement.execute();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			closeConnection(connection);
		}
	}
}