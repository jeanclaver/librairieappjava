/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Librairie.dao;

import Librairie.model.Livre;
import Librairie.model.Categorie;
import java.util.List;

/**
 *
 * @author vagrant
 */
public interface BookDAO {
	public List<Livre> findAllBooks(); //méthode pour lister tous les livres de la base de données.
	
	public List<Livre> searchBooksByKeyword(String keyWord);// permet à l'utilisateur de rechercher des livres par mot-clé dans le titre du livre ou par le prénom et le nom de l'auteur.
	
	public List<Categorie> findAllCategories(); // est requis par l'application pour fournir une liste catégorisée de livres.

	public void insert(Livre book);

	public void update(Livre book);

	public void delete(Long bookId);

}