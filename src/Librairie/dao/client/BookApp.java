/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Librairie.dao.client;

import Librairie.dao.BookDAO;
import Librairie.dao.BookDAOImpl;
import Librairie.model.Livre;
import java.util.List;

/**
 *
 * @author vagrant
 */
public class BookApp {

    private static BookDAO bookDao = new BookDAOImpl();

    public static void main(String[] args) {
       // Liste tous les livres
        System.err.println("Lister tous les livres:");
        findAllBooks();
        System.out.println();
        // recherche livre par mot clé
        System.err.println("Rechercher un livre par mot-clé dans le titre du livre : Java :");
        searchBooks("Java");
        System.out.println();
        System.err.println("Rechercher un livre par mot clé dans le nom de l'auteur : bintou :");
        searchBooks("bangoura");
    }

    private static void findAllBooks() {
        List<Livre> books = bookDao.findAllBooks();
        for (Livre book : books) {
            System.out.println(book);
        }
    }
    
    private static void searchBooks(String keyWord){
        List<Livre> books = bookDao.searchBooksByKeyword(keyWord);
        for (Livre book : books){
             System.out.println(book);
        }
    }
}
